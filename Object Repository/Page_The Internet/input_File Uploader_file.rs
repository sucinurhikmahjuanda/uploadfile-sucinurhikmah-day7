<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_File Uploader_file</name>
   <tag></tag>
   <elementGuidId>d665fa62-36df-4054-b7f7-29db3e426e1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#file-upload</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='file-upload']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>992744a9-8e03-4ff0-b211-40aea0d8a502</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file-upload</value>
      <webElementGuid>2194ac5a-8fc0-4595-9389-d22f3ddc7af6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>a636792d-55c6-4eb7-8794-f51667a3d1e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>a9e269c0-c4c3-466b-8b33-f7b69b7a8bf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;file-upload&quot;)</value>
      <webElementGuid>7c7deb8a-b2f8-4b84-af5d-eeda2e209d6a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='file-upload']</value>
      <webElementGuid>e6dc53b5-d802-422e-b390-4a380927de99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/form/input</value>
      <webElementGuid>ba90e5c3-0606-4ddb-9742-daa53f9fafa7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>df139cf3-1be7-4854-a3ed-941f329c2c39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'file-upload' and @type = 'file' and @name = 'file']</value>
      <webElementGuid>52778710-c2de-4f56-abe5-51fdcbc4397a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
