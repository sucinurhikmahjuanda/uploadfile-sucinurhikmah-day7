<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite for TC002</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>4d1eaa74-dbf1-4bd1-8770-a35014c82c4f</testSuiteGuid>
   <testCaseLink>
      <guid>f997a5cc-2a0f-48fb-b2ab-21cf35c93fca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC002-UploadDataDrivenTesting</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>66d6d69e-42bb-47be-a0ce-3399d0b23558</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataFile TC002</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>66d6d69e-42bb-47be-a0ce-3399d0b23558</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Name</value>
         <variableId>7b70e536-a2ab-4ab9-aacc-876165909011</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>66d6d69e-42bb-47be-a0ce-3399d0b23558</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>LocationFile</value>
         <variableId>ca9a2b15-ba12-4521-89ea-7fa9dba2a4f7</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
